//Packages imported.
import 'package:flutter/material.dart';
//Package imports created.
import '/utils/about_developer.dart';

class AboutScreen extends StatelessWidget{

    @override
    Widget build(BuildContext context) {
        
        return Scaffold(
            appBar: AppBar(
                title: Text('About the Developer'),
                centerTitle: true,
            ),
            body:SingleChildScrollView(
                child: Container(
                    child:Column(
                        children: <Widget>[
                            Stack(
                                // ignore: deprecated_member_use
                                overflow: Overflow.visible,
                                alignment: Alignment.center,
                                children: [
                                    Image.asset('../assets/images/flutter.png',
                                    height: MediaQuery.of(context).size.height/3,
                                    fit:BoxFit.cover),
                                    Positioned(
                                        bottom: -85.0,
                                        child: CircleAvatar(
                                            radius: 100,
                                            backgroundColor: Colors.white,
                                            backgroundImage: AssetImage('../assets/images/mjpc.jpg'),
                                        )
                                    )
                                ],
                            ),
                            SizedBox(
                                height: 80
                            ),devDetails,
                            SizedBox(
                                height: 25
                            ),
                            aboutMe
                        ],
                    ),
                )
            ),
        );
    }
}

