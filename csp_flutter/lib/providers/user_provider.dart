//Packages imported.
import 'package:flutter/foundation.dart';

class UserProvider extends ChangeNotifier {
    String? _accessToken;
    String? _designation;

    UserProvider([ this._accessToken, this._designation ]);

    get accessToken => _accessToken;
    get designation => _designation;

    setAccessToken(String? value) {
        _accessToken = value;
        notifyListeners();
    }

    setDesignation(String? value) {
        _designation = value;
        notifyListeners();
    }
}