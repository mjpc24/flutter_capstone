import 'package:flutter/material.dart';

var lineDivider = Divider(
            color: Colors.black,
            indent: 25,
            endIndent: 25,
);
var devDetails = ListTile(
    title: Center(
        child: Text('Jeph Cobarrubias',
        style:TextStyle(
            fontSize: 35,
            letterSpacing: 1.5,
            color: Color(0xFF022553),
            fontWeight: FontWeight.w600,
            ) 
        )
    ),
    subtitle: Center(
        child: Padding(
            padding: EdgeInsets.only(top: 10),
            child:Text('Fullstack Developer | IT Specialist',
            style: TextStyle(
                fontSize: 16,
                letterSpacing: 1.5,
                color: Color(0xFF2D61A7),
                ) ,
            ) ,
        ),
    )
);
var aboutMe = Column(
    children: [
        Column(
            children: [
                lineDivider,
                Center(
                    child: Text('About Me',
                    style: TextStyle(fontSize: 20,
                    letterSpacing: 1.5,
                    color: Color(0xFF022553),
                    fontWeight: FontWeight.w600))
                ),
                Center(
                    child: SizedBox(
                        width: 450,
                        child: Card(
                            margin: EdgeInsets.all(10.0),
                            child:Padding(
                                padding: const EdgeInsets.all(20.0),
                                child: Text('"A dedicated and goal oriented Full-Stack Web developer. An I.T. professional with 5 years of experience as System/Network Administrator. A team player and a leader. Skilled in written and verbal communication. Capable to understand and speak Japanese (N5 level),capable to read and write katakana, hiragana and 150 characters of Kanji. Has a positive attitude towards work."',textAlign: TextAlign.center,style: TextStyle(fontSize: 20)),
                            ),
                        ),
                    )
                ),SizedBox(
                    height: 25
                ),lineDivider,
            ],
        ),Column(
            children: [
                Center( 
                    child: Text('Skills',
                    style: TextStyle(fontSize: 20,
                    letterSpacing: 1.5,
                    color: Color(0xFF022553),
                    fontWeight: FontWeight.w600))
                ),
                Center(
                    child: SizedBox(
                        width: 450,
                        child: Card(
                            margin: EdgeInsets.all(10.0),
                            child:Padding(
                                padding: const EdgeInsets.all(20.0),
                                child: Container(
                                    child: Wrap(
                                        crossAxisAlignment: WrapCrossAlignment.center,
                                        children: [
                                            Center(
                                                child: Column(
                                                    children: [
                                                        Text('Web and Mobile Development',textAlign: TextAlign.center,style: TextStyle(fontSize: 20)),
                                                        Text('Network and Network Infrastructure',textAlign: TextAlign.center,style: TextStyle(fontSize: 20)),
                                                    ],
                                                ),
                                            )
                                        ],
                                    ),
                                ),
                            ),
                            
                        ),
                    )
                ),SizedBox(
                    height: 25
                ),
            ],
            //start contacts
        ),Column(
            children: [
                Center(
                    child: Text('Contact Me',
                    style: TextStyle(fontSize: 20,
                    letterSpacing: 1.5,
                    color: Color(0xFF022553),
                    fontWeight: FontWeight.w600))
                ),
                Center(
                    child: SizedBox(
                        width: 450,
                        child: Card(
                            margin: EdgeInsets.all(10.0),
                            child:Padding(
                                padding: const EdgeInsets.all(20.0),
                                child: Container(
                                    child: Wrap(
                                        crossAxisAlignment: WrapCrossAlignment.center,
                                        children: [
                                            Icon(Icons.add_call),
                                            Text('(+63)917-****-***',textAlign: TextAlign.center,style: TextStyle(fontSize: 20)),
                                        ],
                                    ),
                                ),
                            ),
                            
                        ),
                    )
                ),Center(
                    child: SizedBox(
                        width: 450,
                        child: Card(
                            margin: EdgeInsets.all(10.0),
                            child:Padding(
                                padding: const EdgeInsets.all(20.0),
                                child: Container(
                                    child: Wrap(
                                        crossAxisAlignment: WrapCrossAlignment.center,
                                        children: [
                                            Icon(Icons.add_location),
                                            Text('Binangonan, Rizal',textAlign: TextAlign.center,style: TextStyle(fontSize: 20)),
                                        ],
                                    ),
                                ),
                            ),
                        ),
                    )
                ),SizedBox(
                    height: 20
                ),lineDivider,
            ],
        )
    ],
);