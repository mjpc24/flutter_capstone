//Packages imported.
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
//personal imports created.
import '/providers/user_provider.dart';

class AppDrawer extends StatelessWidget {
    @override
    Widget build(BuildContext context) {

        return Drawer(
            child: Container(
                width: double.infinity,
                color: Color.fromRGBO(20, 45, 68, 1),
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                        DrawerHeader(
                            child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                    SizedBox(height: 36.0),
                                    Image.asset('assets/ffuf-logo.png', width: 100,color: Colors.white,), // Modify width and color of Image widget to match given sample.
                                    Container(
                                        margin: EdgeInsets.only(top: 16.0),
                                        child: Column(
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: [
                                                Text('FFUF Project Management', style: TextStyle(fontSize: 20.0, color: Colors.white)),
                                                Text('Made by FFUF Internal Dev Team', style: TextStyle(color: Colors.white)),
                                            ]
                                        )
                                    )
                                ]
                            )
                        ),
                        Spacer(),
                        Column(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                                ListTile(
                                    title: Text('About', style: TextStyle(color: Colors.white)),
                                    onTap: () async {
                                        Navigator.pushNamed(context, '/about');
                                    }
                                ),
                                ListTile(
                                    title: Text('Logout', style: TextStyle(color: Colors.white)),
                                    onTap: () async {
                                        Provider.of<UserProvider>(context, listen: false).setAccessToken(null);
                                        Provider.of<UserProvider>(context, listen: false).setDesignation(null);
                                        SharedPreferences prefs = await SharedPreferences.getInstance();
                                        prefs.remove('accessToken');
                                        prefs.remove('userId');
                                        Navigator.pushNamedAndRemoveUntil(context, '/', (Route<dynamic> route) => false);
                                    }
                                ) 
                            ]
                        )
                    ]
                )
            )
        );
    }
}