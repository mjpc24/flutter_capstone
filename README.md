# Project Management App

# Project Overview

---

The project aims to provide project management for contractors, sub-contractors and assembly team.

# Getting Started

---

### Clone the project

```jsx
git clone https://mjpc24@bitbucket.org/mjpc24/flutter_capstone.git
```

### Open project using IDE

You may opt to use your preferred IDE but Visual Studio Code is preferred

### Get the packages

Open the pubspec.yaml and save the file to apply the content dependencies

### Create a .env file

Write the following API credentials and save

`API_SERVER_URL=http://127.0.0.1:4000/api`
`STATIC_SERVER_URL=http://10.0.2.2:4001`

### Run the project

First, open the and run the API using the terminal command `dart run bin/main.dart` to run the API server locally then click `F5` or debug to start the `csp_flutter` project

**Note:** You may opt to use chrome or android emulator to run the project interface

## Flutter Version

---

Flutter 2.2.3

sdk: ">=2.12.0 <3.0.0"

## Packages Installed

---

[http: ^0.13.3](https://pub.dev/packages/http)

This package contains a set of high-level functions and classes that make it easy to consume HTTP resources. It's multi-platform, and supports mobile, desktop, and the browser.

[provider:^5.0.0](https://pub.dev/packages/provider) 

A wrapper around InheritedWidget to make them easier to use and more reusable.

[shared_preferences: ^2.0.6](https://pub.dev/packages/shared_preferences)

Wraps platform-specific persistent storage for simple data (NSUserDefaults on iOS and macOS, SharedPreferences on Android, etc.). Data may be persisted to disk asynchronously, and there is no guarantee that writes will be persisted to disk after returning, so this plugin must not be used for storing critical data.

[image_picker: ^0.8.0+3](https://pub.dev/packages/image_picker)

A Flutter plugin for iOS and Android for picking images from the image library, and taking new pictures with the camera.

[flutter_dotenv: ^5.0.0](https://pub.dev/packages/flutter_dotenv)

An *environment* is the set of variables known to a process (say, `PATH`, `PORT`, ...). It is desirable to mimic the production environment during development (testing, staging, ...) by reading these values from a file.

This library parses that file and merges its values with the built-in map.

[email_validator: ^2.0.1](https://pub.dev/packages/email_validator)

A simple (but correct) Dart class for validating email addresses without using RegEx. Can also be used to validate emails within Flutter apps (see Flutter email validation).

## Features

---

### Login Feature

- Save token to app state
- Save token to local storage

### Contractor

- Show all projects
- Add project
- Assign project subcontractor
- Review finished project tasks

### Subcontractor

- Show assigned projects
- Show project tasks
- Add task & assign to assembly team

### Assembly Team

- Show projects with assigned tasks
- Show project tasks
- Tag task as ongoing
- Tag task as completed

### User Test Credentials

| Email  | Password |
| ------------- | ------------- |
| contractor@gmail.com  | contractor  |
| subcontractor@gmail.com  | subcontractor  |
| assembly-team@gmail.com  | assembly-team  |